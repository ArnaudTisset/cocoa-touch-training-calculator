//
//  JLGCalculatorViewController.m
//  Calculator
//
//  Created by Jlformation on 5/02/14.
//  Copyright (c) 2014 Jlformation. All rights reserved.
//

#import "JLGCalculatorViewController.h"
#import "JLGCalcOperation.h"

@interface JLGCalculatorViewController ()

@property (weak, nonatomic) IBOutlet UITextField *resultTestField;
@property JLGCalcOperation *currentOperation;
@property JLGCalcOperation *lastOperation;
- (IBAction)touchCalcAction:(UIButton *)sender;
- (IBAction)touchSaveAction:(id)sender;
@end



@implementation JLGCalculatorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _currentOperation = [[JLGCalcOperation alloc] init];
    NSError *fmError;
    NSString *filePath = @"~/Documents/result.txt";
    NSString *fileContent = [NSString stringWithContentsOfFile:[filePath stringByExpandingTildeInPath]encoding:NSUTF16StringEncoding error: &fmError];
                             
                             
    NSLog(@"file content: %@", fileContent);
    NSLog(@"error: %@", fmError);
    _resultTestField.text = fileContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchCalcAction:(UIButton *)sender {
    
    NSLog(@"Key touched: %@", sender);
    


    
    // NSLog(@"equal: %d", [sender.currentTitle compare:@"C"]);
    if([sender.currentTitle compare:@"C"] == NSOrderedSame)
    {
        _resultTestField.text = @"";
        [_currentOperation reset];
    } else if( ([sender.currentTitle compare:@"+"] == NSOrderedSame)
        ||  ([sender.currentTitle compare:@"-"] == NSOrderedSame)
        ||  ([sender.currentTitle compare:@"/"] == NSOrderedSame)
        ||  ([sender.currentTitle compare:@"*"] == NSOrderedSame) )
        
    {
       [_currentOperation setOperatorWithString:sender.currentTitle];
    } else if( [sender.currentTitle compare:@"="] == NSOrderedSame)
    {
        _resultTestField.text = [NSString stringWithFormat:@"Resultat: %f", [_currentOperation calc]];
        _lastOperation = [_currentOperation copy];
        [_currentOperation reset];
        
    } else {
        // operande
        if(_currentOperation.operator == JLGDefault) _currentOperation.oper1 = [sender.currentTitle floatValue];
        else _currentOperation.oper2 = [sender.currentTitle floatValue];
        
        self.resultTestField.text = sender.currentTitle;
    }
}

- (IBAction)touchSaveAction:(id)sender {
    if(_lastOperation != nil && _lastOperation.operator != JLGDefault)
    {
        
        NSString *strOperator;
        if(_lastOperation.operator == JLGAdd) strOperator        = [NSString stringWithFormat:@"+"];
        else if(_lastOperation.operator == JLGSous) strOperator  = [NSString stringWithFormat:@"-"];
        else if(_lastOperation.operator == JLGDiv) strOperator   = [NSString stringWithFormat:@"/"];
        else if(_lastOperation.operator == JLGMult) strOperator = [NSString stringWithFormat:@"*"];
        else [NSException raise:@"Invalid operator value" format:@"operator %u is invalid", _lastOperation.operator];
        
        NSString * fileContent = [NSString stringWithFormat:@"Operation : %f %@ %f = %f", _lastOperation.oper1, strOperator, _lastOperation.oper2, _lastOperation.res];
        NSString * filePath = [NSString stringWithFormat:@"~/Documents/result.txt"];
        NSFileManager * fm = [[NSFileManager alloc] init];
        
        NSError * fmError;
        
        //NSLog(@"%@", [fm attributesOfItemAtPath:filePath error: &fmError]);
        
        //fh
        [fileContent writeToFile:[filePath stringByExpandingTildeInPath] atomically:YES encoding:NSUTF16StringEncoding error: &fmError];
        
        NSLog(@"%@", fmError);
        NSLog(@"%@", [filePath stringByExpandingTildeInPath]);
        
    }
}
@end
