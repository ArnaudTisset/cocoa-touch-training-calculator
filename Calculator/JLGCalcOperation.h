//
//  JLGCalcOperation.h
//  Calculator
//
//  Created by Jlformation on 5/02/14.
//  Copyright (c) 2014 Jlformation. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum JLGOperatorTypes {
    JLGDefault  = 0,
    JLGAdd  = 1,
    JLGSous = 2,
    JLGMult = 3,
    JLGDiv  = 4
}JLGOperator;

@interface JLGCalcOperation : NSObject
@property (nonatomic) float oper1;
@property (nonatomic) float oper2;
@property (nonatomic) JLGOperator operator;
@property float res;
- (float) calc;
-(void)setOperatorWithString:(NSString *)operator;
-(void)reset;
@end
