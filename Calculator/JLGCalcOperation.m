//
//  JLGCalcOperation.m
//  Calculator
//
//  Created by Jlformation on 5/02/14.
//  Copyright (c) 2014 Jlformation. All rights reserved.
//

#import "JLGCalcOperation.h"

@implementation JLGCalcOperation
- (float) calc
{
    switch (self.operator) {
        case JLGAdd:
            self.res = self.oper1 + self.oper2;
            break;
        case JLGSous:
            self.res = self.oper1 - self.oper2;
            break;
        case JLGMult:
            self.res = self.oper1 * self.oper2;
            break;
        case JLGDiv:
            if(self.oper2 == 0L) [NSException raise:@"Invalid operation" format:@"div by zero"];
            self.res = self.oper1 / self.oper2;
            break;
        default:
            break;
    }
    return self.res;
}

-(JLGCalcOperation *) init
{
    JLGCalcOperation * operation = [super init];
    operation.operator = JLGDefault;
    operation.oper1 = 0L;
    operation.oper2 = 0L;
    operation.res   = 0L;
    return operation;
}

-(JLGCalcOperation *) initWithOperation:(JLGCalcOperation *)oper
{
    JLGCalcOperation * operation = [super init];
    
    operation.operator = oper.operator;
    operation.oper1    = oper.oper1;
    operation.oper2    = oper.oper2;
    operation.res      = oper.res;
    return operation;
}
-(void)setOperator:(JLGOperator)operator
{
    _operator = operator;
}

-(void)setOperatorWithString:(NSString *)operator
{
    if([operator compare: @"+"] == NSOrderedSame) self.operator = JLGAdd;
    else if([operator compare: @"-"] == NSOrderedSame) self.operator = JLGSous;
    else if([operator compare: @"*"] == NSOrderedSame) self.operator = JLGMult;
    else if([operator compare: @"/"] == NSOrderedSame) self.operator = JLGDiv;
    else [NSException raise:@"Invalid operator value" format:@"operator %@ is invalid", operator];
}
-(void)reset
{
    _oper1 = 0L;
    _oper2 = 0L;
    _res   = 0L;
    _operator = JLGDefault;
}

-(JLGCalcOperation *) copy
{
    JLGCalcOperation * newOperation = [[JLGCalcOperation alloc] initWithOperation:self];
    return newOperation;
}
@end
